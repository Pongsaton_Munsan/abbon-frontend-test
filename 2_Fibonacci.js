function fibonacci(nums) {
  let fib = [0, 1];
  let data = [];
  let i = 2;

  while (true) {
    fib[i] = fib[i - 1] + fib[i - 2];
    if (fib[i] > nums) break;
    data.push(fib[i]);
    i++;
  }

  return data;
}

console.log(fibonacci(15));
console.log(fibonacci(8));
