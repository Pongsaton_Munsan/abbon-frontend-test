function integerToRoman(num) {
  const roman = { M: 1000, D: 500, C: 100, L: 50, X: 10, V: 5, I: 1 };
  // const roman = {
  //   M: 1000,
  //   CM: 900,
  //   D: 500,
  //   CD: 400,
  //   C: 100,
  //   XC: 90,
  //   L: 50,
  //   XL: 40,
  //   X: 10,
  //   IX: 9,
  //   V: 5,
  //   IV: 4,
  //   I: 1,
  // };
  let str = "";

  if (num < 1 || num > 3999) return "Wrong Input";

  for (let key in roman) {
    const divide = Math.floor(num / roman[key]);
    num -= divide * roman[key];
    str += key.repeat(divide);
  }

  return str;
}

console.log(integerToRoman(3));
console.log(integerToRoman(58));
console.log(integerToRoman(1994));
