function reverseInteger(num) {
  return (
    Math.sign(num) * parseInt(num.toString().match(/\d/g).reverse().join(""))
  );
}

console.log(reverseInteger(123));
console.log(reverseInteger(-321));
console.log(reverseInteger(120));
