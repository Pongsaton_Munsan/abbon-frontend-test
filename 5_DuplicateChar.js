function checkDuplicateChar(str) {
  const strArr = str
    .toLowerCase()
    .split("")
    .sort()
    .join("")
    .match(/(.)\1+/g);
  if (strArr == null) {
    return 0;
  }
  return strArr.length;
}

console.log(checkDuplicateChar("abcde"));
console.log(checkDuplicateChar("aabbcde"));
console.log(checkDuplicateChar("aabBcde"));
console.log(checkDuplicateChar("indivisibility"));
console.log(checkDuplicateChar("Indivisibilities"));
console.log(checkDuplicateChar("aA11"));
console.log(checkDuplicateChar("ABBA"));
