function checkValidBracket(str) {
  const bucket = {
    ")": "(",
    "}": "{",
    "]": "[",
  };
  let stack = [];

  for (let i = 0; i < str.length; i++) {
    if (str[i] == "(" || str[i] == "{" || str[i] == "[") {
      stack.push(str[i]);
    } else if (stack[stack.length - 1] == bucket[str[i]]) {
      stack.pop();
    } else {
      return false;
    }
  }

  return stack.length == 0;
}

console.log(checkValidBracket("(){}[]"));
console.log(checkValidBracket("([{}])"));
console.log(checkValidBracket("(}"));
console.log(checkValidBracket("[(])"));
console.log(checkValidBracket("[({})](]"));
