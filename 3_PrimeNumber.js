function getPrimeNumber(num) {
  let array = [];
  for (let i = 2; i < Infinity; i++) {
    let isPrime = true;
    for (let j = 2; j < i; j++) {
      if (i % j == 0) {
        isPrime = false;
        break;
      }
    }
    if (isPrime) {
      array.push(i);
      if (array.length == num) break;
    }
  }

  return array;
}

console.log(getPrimeNumber(3));
console.log(getPrimeNumber(6));
