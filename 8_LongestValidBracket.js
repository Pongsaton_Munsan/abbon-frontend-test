function checkValidBracket(str) {
  let stack = [];
  let count = 0;

  for (let i = 0; i < str.length; i++) {
    if (str[i] == "(") {
      stack.push(str[i]);
    } else if (stack[stack.length - 1] == "(") {
      stack.pop();
      count += 2;
    }
  }

  return count;
}

console.log(checkValidBracket("(()"));
console.log(checkValidBracket(")()()"));
console.log(checkValidBracket(""));
