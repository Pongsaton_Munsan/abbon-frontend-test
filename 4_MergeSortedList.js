function mergeSortedList(arr1, arr2) {
  return arr1.concat(arr2).sort();
}

console.log(mergeSortedList([1, 2, 4], [1, 3, 4]));
console.log(mergeSortedList([], []));
console.log(mergeSortedList([], [0]));
